import api.mod.StarMod;

public class StarAPIMod extends StarMod {
    public static void main(String[] args) {

    }
    @Override
    public void onGameStart() {
        setModName("StarAPI").setModVersion("0.1");
        setModDescription("A mod containing many wrappers and systems to make modding easier to approach");
    }
}
