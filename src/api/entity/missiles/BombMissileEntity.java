package api.entity.missiles;

import org.schema.game.common.data.missile.BombMissile;

public class BombMissileEntity extends MissileEntity {

    public BombMissileEntity(BombMissile missile) {
        super(missile);
    }

    @Override
    public BombMissile getInternalMissile() {
        return (BombMissile) super.getInternalMissile();
    }
}
