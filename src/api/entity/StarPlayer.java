package api.entity;

import api.common.GameServer;
import api.element.inventory.Inventory;
import api.element.inventory.InventoryType;
import api.faction.StarFaction;
import api.server.Server;
import api.universe.StarSector;
import api.universe.StarUniverse;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.RegisteredClientOnServer;

import java.util.Map;
import java.util.Set;

public class StarPlayer {

    private PlayerState playerState;

    public StarPlayer(PlayerState state) {
        playerState = state;
    }

    public StarFaction getFaction() {
        GameServerState gameServerState = GameServerState.instance;
        int factionID = playerState.getFactionId();
        return new StarFaction(gameServerState.getFactionManager().getFaction(factionID));
    }

    public int getCredits() {
        return getPlayerState().getCredits();
    }

    public void setCredits(int credits) {
        getPlayerState().setCredits(credits);
    }

    public String getName() {
        return getPlayerState().getName();
    }

    public void setName(String name) {
        getPlayerState().setName(name);
    }

    public void sendServerMessage(String message){
        Server.sendMessage(this.getPlayerState(), message);
    }

    public void sendMail(String from, String title, String contents) {
        playerState.getClientChannel().getPlayerMessageController().serverSend(from, playerState.getName(), title,
                contents);
    }
    public RegisteredClientOnServer getServerClient(){
        return GameServer.getServerState().getClients().get(getPlayerState().getClientId());
    }
    public int getId(){
        return getPlayerState().getId();
    }

    public Inventory getInventory() {
        return new Inventory(getPlayerState().getInventory(), this, InventoryType.PLAYER_INVENTORY);
    }

    public PlayerState getPlayerState() {
        return this.playerState;
    }

    public StarEntity getCurrentEntity(){
        Set<ControllerStateUnit> units = getPlayerState().getControllerState().getUnits();
        if(units.isEmpty()){
            return null;
        }
        ControllerStateUnit unit = units.iterator().next();
        if(unit != null && unit.playerControllable instanceof SegmentController){
            return new StarEntity((SegmentController) unit.playerControllable);
        }
        return null;
    }

    private PlayerState getPlayerStateFromName(String playerName) {
        GameServerState gameServerState = GameServerState.instance;
        Map<String, PlayerState> playerStates = gameServerState.getPlayerStatesByName();
        PlayerState pState = null;
        try {
            pState = playerStates.get(playerName);
        } catch(Exception e) {
            System.err.println("[StarLoader API]: Tried to get a PlayerState from name, but specified player was not found on server!");
            e.printStackTrace();
        }
        this.playerState = pState;
        return playerState;
    }

    public void openInventory(Inventory inventory) {

    }

    public StarSector getSector() {
        /**
         * Gets the player's current sector.
         */
        return StarUniverse.getUniverse().getSector(playerState.getCurrentSector());
    }


}
