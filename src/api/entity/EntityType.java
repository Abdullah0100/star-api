package api.entity;

public enum EntityType {
    SHIP,
    STATION,
    SHOP,
    ASTEROID,
    PLANETCORE,
    PLANETSEGMENT,
}
